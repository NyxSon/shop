﻿namespace Shop1;

public class Product
{
    public List<Categories> categor;

    public Product()
    {
        categor = new List<Categories>();
        PopulateList();
    }

    
    private void PopulateList()
    {
        categor.Add(new Categories()
        {
            id = 1,
            name = "Компьютеры",
            Items = new List<Item>()
            {
                new Item(1,"HyperPC", 10000),
                new Item(2, "MSI", 15000),
                new Item(3,"Aser", 20000)
            }
        });
        categor.Add(new Categories()
        {
            id = 2,
            name = "Мониторы",
            Items = new List<Item>()
            {
                new Item(4, "Sumsung", 5000),
                new Item(5,"Benq", 7000),
                new Item(6,"Asus", 9000)
            }
        });
        categor.Add(new Categories()
        {
            id = 3,
            name = "Переферия",
            Items = new List<Item>()
            {
                new Item(7,"мышь Logitech", 2000),
                new Item(8,"мышь Razer", 4000),
                new Item(9,"микрофон Sven", 1000)
            }
        });
    }
}