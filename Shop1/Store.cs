﻿namespace Shop1;

public class Store
{
    static Product product = new Product();
    public static void PrintCategories()
    {
        Console.WriteLine($"Выберите категорию:");
        foreach (Categories c in product.categor)
        {
            Console.WriteLine($"{c.id} - {c.name}");
        }
    }

    public static void PrintProduct()
    {
        int inputCategories = Convert.ToInt32(Console.ReadLine());
        switch (inputCategories)
        {
            case 1:
                PrintList(inputCategories);
                break;
            case 2:
                PrintList(inputCategories);
                break;
            case 3:
                PrintList(inputCategories);
                break;
        }
    }

    private static void PrintList(int inputCategories)
    {
        foreach (Categories c in product.categor)
        {
            if (inputCategories == c.id)
            {
                foreach (Item a in c.Items)
                {
                    Console.WriteLine($"ID - {a.id}, Наименование - {a.name}, Цена - {a.price}");
                }
            }
        }
    }
}