﻿namespace Shop1;

public class Order
{
    static Product product = new Product();
    public List<ItemOrder> itemOrders;

    private static Order item = new Order();

    public Order()
    {
        itemOrders = new List<ItemOrder>();
    }
    
    public static void CreateOrder()
    {
        int sum = 0;
        int i = 1;
        while (i==1)
        {
            Console.WriteLine("Напишите ID товара для добавления в заказ.");
            int IDProduct = Convert.ToInt32(Console.ReadLine());
            foreach (Categories c in product.categor)
            {
                foreach (Item a in c.Items)
                {
                    if (IDProduct == a.id)
                    {
                        Console.WriteLine($"ID - {a.id}, Наименование - {a.name}, Цена - {a.price}");
                        sum += a.price;
                        OrderItem(a.id, a.name, a.price);
                    }
                }
            }
            Console.WriteLine("\n1 - Продолжить \n2 - Выход");
            i = Convert.ToInt32(Console.ReadLine());
        }
        Console.WriteLine("Заказ:");
        foreach (ItemOrder b in item.itemOrders)
        {
            Console.WriteLine($"ID - {b.id}, Наименование - {b.name}, Цена - {b.price}");
        }
        Console.WriteLine($"Цена заказа: {sum}");
    }

    private static void OrderItem(int a, string b, int c)
    {
        item.itemOrders.Add(new ItemOrder()
        {
            id = a,
            name = b,
            price = c
        });
    }
}