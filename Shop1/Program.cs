﻿using Shop1;

public static class Program
{
    static void Main()
    {
        Console.WriteLine("Выберите пункт: \n1 - Категории товаров \n2 - Заказ");
        int input = Convert.ToInt32(Console.ReadLine());
        if (input == 1)
        {
            Store.PrintCategories();
            Store.PrintProduct();
        }
        else if(input == 2)
        {
            Order.CreateOrder();
        }
    }
}