﻿namespace Shop1;

public class Categories
{
    public int id { get; set; }
    public string name { get; set; }

    public List<Item> Items { get; set; } = new List<Item>();
}