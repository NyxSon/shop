﻿namespace Shop1;

public class Item
{
    public int id { get; set; }
    public string name { get; set; }
    public int price { get; set; }

    public Item(int id, string name, int price)
    {
        this.id = id;
        this.name = name;
        this.price = price;
    }
}